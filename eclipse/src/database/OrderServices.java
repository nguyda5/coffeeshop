/**
 * @author David Nguyen
 * @author Sirine Aoudj
 */
package database;

import java.sql.*;

public class OrderServices {
	
	private Connection con; 
	private String orderID;
	
	public OrderServices(Connection con) throws ClassNotFoundException {
		this.con = con;
		this.orderID = "0000";
	}
	
	/**
	 * This method creates an order in the order table with their user name
	 * @param username
	 * @throws SQLException
	 */
	public void createOrder(String username) throws SQLException {
		try {
			String SQL = "{call createOrder(?,?)}";
			CallableStatement cstmt = con.prepareCall(SQL);
			cstmt.setString(1,username);
			cstmt.registerOutParameter(2, Types.VARCHAR);
			cstmt.execute();
			this.orderID = cstmt.getString(2);
			con.commit();
			
			System.out.println("Inserted into ORDERS: " + username);
		}catch(SQLException e) {
			con.rollback();
			e.printStackTrace();
			System.out.println("error! rollback");
		}
	}
	/**
	 * This method returns the system date to get the date that the order was created
	 * @return
	 * @throws SQLException
	 */
	public String getOrderDate() throws SQLException{
		String date = "";
		try {
			String SQL = "{? = call getOrderDate(?)}";
			CallableStatement cstmt = con.prepareCall(SQL);
			cstmt.registerOutParameter(1, Types.DATE);
			cstmt.setString(2, this.orderID);
			cstmt.execute();
			date = cstmt.getString(1);

		}catch(SQLException e) {
			con.rollback();
			e.printStackTrace();
			System.out.println("error! rollback");
		}
		return date;
	}
	
	/**
	 *This method returns the delivery date of the order, so 24h after the order was placed 
	 * @return
	 * @throws SQLException
	 */
	public String getDeliveryDate() throws SQLException{
		String date = "";
		try {
			String SQL = "{? = call getDeliveryDate(?)}";
			CallableStatement cstmt = con.prepareCall(SQL);
			cstmt.registerOutParameter(1, Types.DATE);
			cstmt.setString(2, this.orderID);
			cstmt.execute();
			date = cstmt.getString(1);

		}catch(SQLException e) {
			con.rollback();
			e.printStackTrace();
			System.out.println("error! rollback");
		}
		return date;
	}
	public String getOrderID() {
		return this.orderID;
	}
}
