/**
 * @author David Nguyen
 * @author Sirine Aoudj
 * This class allows to
 */
package database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import objects.*;
import connection.DBConnection;
import javafx.scene.control.Label;

public class CustomerServices {
	private Connection con; 
	
	public CustomerServices(Connection con) throws ClassNotFoundException {
		this.con = con;
	}
	
	/**
	 * This method creates a customer in the database 
	 * @param username is the username of the user
	 * @param cust is the customer object
	 * @param messageToUser is the message that outputs in the application
	 * @throws SQLException
	 */
	public void addCustomer(String username, Customer cust, Label messageToUser) throws SQLException {
		try {
			/*First it creates a customer in the database using the information of the customer object and the user name*/
			String userId = getUserID(username);
			String SQL = "{call createCustomer(?,?,?,?,?)} ";
			CallableStatement cstmt = con.prepareCall(SQL);
			cstmt.setString(1, userId);
			cstmt.setString(2, cust.getDeliveryAddress());
			cstmt.setString(3, cust.getPhoneNumber());
			cstmt.setString(4, cust.getEmailAddress());
			cstmt.setInt(5, 0);
			cstmt.execute();
			System.out.println("Inserted into CUSTOMER: " + username);
			con.commit();
			messageToUser.setText("User has been created!");;
			/*If the customer already exist it will display in the application that this user has already been created */
		}catch(SQLException e) {
			con.rollback();
			messageToUser.setText("User already exist!");;
			System.out.println("Error: Customer already exist!");
		}
	}
	
	/**
	 * This adds a reference to the customer object when a user has been created and uses their user name as reference, as well as in the database
	 * @param username is the user name of the user
	 * @param cust is the customer object
	 * @throws SQLException
	 */
	public void addReference(String username, Customer cust) throws SQLException{
		String customerID = getCustomerID(username);
		String refID = getCustomerID(cust.getReferred());
		if(!customerID.equals("0000") && !refID.equals("0000")) {
			try {
				/*Fist it select the number of references that user has*/
				String SQL = "SELECT COUNT(reference_id) FROM customer_reference WHERE customer_id = '"+refID+"'";
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(SQL);
				int referred = 0;
				while(rs.next()) {
					referred = rs.getInt("COUNT(reference_id)");
				}
				/*If the user already doesn't has referred 3 people then it will add the reference in the database*/
				if(referred < 3) {
					SQL = "{call addReference(?,?)}";
					CallableStatement cstmt = con.prepareCall(SQL);
					cstmt.setString(1, refID);
					cstmt.setString(2, customerID);
					cstmt.executeUpdate();
					System.out.println("Inserted into CUSTOMER_REFERENCE: " + customerID);
					con.commit();
				}
			}catch(SQLException e) {
				con.rollback();
				System.out.println("Error: Cannot add reference!");
			}
		}else {
			/*This will print out if someone tries to refer someone that does not exists*/
			System.out.println("Customer/Reference does not exist!");
		}
	}
	
	/**
	 * This methods gets the amount of discounts that the customers has according to the number of people that user referred
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public int getDiscounts(String username) throws SQLException{
		String customerID = getCustomerID(username);
		int count = 0;
		if(!customerID.contentEquals("0000")) {
			try {
				String SQL = "SELECT COUNT(reference_id) FROM customer_reference WHERE customer_id = '"+customerID+"'";
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(SQL);
				while(rs.next()) {
					count = rs.getInt("COUNT(reference_id)");
				}
				SQL = "SELECT discount_used FROM customer WHERE customer_id = '"+customerID+"'";
				rs = st.executeQuery(SQL);
				while(rs.next()) {
					count = count - rs.getInt("discount_used");
				}
			}catch(SQLException e) {
				con.rollback();
				System.out.println("Error: Cannot add reference!");
			}
		}
		
		return count;
	}
	
	/**
	 * This method returns the userId of that user
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public String getUserID(String username) throws SQLException {
		String userID = "0000";
		try {
			CallableStatement cstmt = 
				con.prepareCall("{? = call getUserId( ? )}");
			cstmt.registerOutParameter(1, Types.VARCHAR);
			cstmt.setString(2, username);
			cstmt.executeUpdate();
			userID = cstmt.getString(1);
		}catch(SQLException e) {
			System.out.println("userid does not exist!");
		}
		return userID;
	}
	
	/**
	 * This method returns the customer id of that user
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public String getCustomerID(String username) throws SQLException {
		String customerID = "0000";
		try {
			CallableStatement cstmt = 
				con.prepareCall("{? = call getCustomerId( ? )}");
			cstmt.registerOutParameter(1, Types.VARCHAR);
			cstmt.setString(2, username);
			cstmt.executeUpdate();
			customerID = cstmt.getString(1);
		}catch(SQLException e) {
			System.out.println("customerid does not exist!");
		}
		return customerID;
	}
}
