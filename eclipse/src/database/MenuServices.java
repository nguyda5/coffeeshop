/**
 * @author David Nguyen
 * @author Sirine Aoudj
 */
package database;

import java.sql.*;
import java.util.*;
import objects.*;

public class MenuServices {
	private Connection con; 
	
	public MenuServices(Connection con) throws ClassNotFoundException {
		this.con = con;
	}
	
	/**
	 * This method adds a menu into the database associated with the username
	 * @param username
	 * @param currentMenu
	 * @throws SQLException
	 */
	public void addMenu(String username, Menu currentMenu) throws SQLException {
		String menuID = "0000";
		try {
			//First it assigns the menu id to the customer id in the menu table
			String SQL = "{call assignCustomerMenu(?,?)}";
			CallableStatement cstmt = con.prepareCall(SQL);
			cstmt.setString(1, username);
			cstmt.registerOutParameter(2, Types.VARCHAR);
			cstmt.execute();
			menuID = cstmt.getString(2);
			currentMenu.addMenuID(menuID);
			con.commit();
			System.out.println("Inserted into MENU: " + menuID);
		}catch(SQLException e) {
			con.rollback();
			e.printStackTrace();
			System.out.println("error! rollback");
		}
		//Then it assign each products id of that menu to the menu id in the menu products table
		List<Product> allProducts = currentMenu.getProducts();
		List<Integer> allQuantities = currentMenu.getQuantities();
		for(int i = 0; i < allProducts.size(); i++) {
			try {
				String SQL = "{call assignProductsMenu(?,?,?)}";
				CallableStatement cstmt = con.prepareCall(SQL);
				cstmt.setString(1, menuID);
				cstmt.setString(2, allProducts.get(i).getProductId());
				cstmt.setInt(3, allQuantities.get(i));
				cstmt.execute();
				con.commit();
				System.out.println("Inserted into MENUPRODUCTS: " + menuID);
			}catch(SQLException e) {
				con.rollback();
				e.printStackTrace();
				System.out.println("error! rollback");
			}
		}
	}
}
