/**
 * @author David Nguyen
 * @author Sirine Aoudj
 * This class contains all the objects that are necessary for an user when you login
 */
package database;

import java.sql.Connection;

import connection.DBConnection;
import objects.*;
import security.UserCredentials;

public class UserInstance {
	private DBConnection db = new DBConnection();
	private Connection con = db.getConnection();
	//These are all the objects that a user has when he logs in
	private UserCredentials uc;
	private CustomerServices cs;
	private ProductServices ps;
	private MenuServices ms;
	private OrderServices os; 
	private ShoppingCartServices scs;
	private String username;
	private boolean loggedIn;
	private Menu currentMenu;
	private ShoppingCart currentCart;
	
	/**
	 * This creates all the object with the connection as they all require a connection to have access to the pl/sql
	 * @throws ClassNotFoundException
	 */
	public UserInstance() throws ClassNotFoundException {
		this.uc = new UserCredentials(con);
		this.cs = new CustomerServices(con);
		this.ps = new ProductServices(con);
		this.ms = new MenuServices(con);
		this.os = new OrderServices(con);
		this.scs = new ShoppingCartServices(con,os);
		this.currentMenu = new Menu();
		this.currentCart= new ShoppingCart();
		username = "";
		loggedIn = false;
	}
	
	/**
	 * This method resets the shopping cart, empties it, after creating a shopping cart
	 * @throws ClassNotFoundException
	 */
	public void resetCart() throws ClassNotFoundException {
		currentCart = new ShoppingCart();
	}
	
	/**
	 * This method resets the menu, empties it, after creating a menu
	 * @throws ClassNotFoundException
	 */
	public void resetMenu() throws ClassNotFoundException {
		currentMenu = new Menu();
	}
	
	/**
	 *This allows to get the shopping cart of the user 
	 * @return
	 */
	public ShoppingCart getCart() {
		return this.currentCart;
	}
	
	/**
	 *This allows to get the current menu of the user 
	 * @return
	 */
	public Menu getMenu() {
		return this.currentMenu;
	}
	
	/**
	 *This allows to get the user's credentials of the user 
	 * @return
	 */
	public UserCredentials getUc() {
		return uc;
	}
	
	/**
	 *This allows to set the user's credentials of the user 
	 * @return
	 */
	public void setUc(UserCredentials uc) {
		this.uc = uc;
	}
	/**
	 *This allows to get the customer services of the user 
	 * @return
	 */
	
	public CustomerServices getCs() {
		return cs;
	}
	
	/**
	 *This allows to set the customer services of the user 
	 * @return
	 */
	public void setCs(CustomerServices cs) {
		this.cs = cs;
	}
	
	/**
	 *This allows to get the product services of the user 
	 * @return
	 */
	public ProductServices getPs() {
		return ps;
	}
	
	/**
	 *This allows to set the product services of the user 
	 * @return
	 */
	public void setPs(ProductServices ps) {
		this.ps = ps;
	}
	
	/**
	 *This allows to get the menu services of the user 
	 * @return
	 */
	public MenuServices getMs() {
		return ms;
	}
	
	/**
	 *This allows to set the menu services of the user 
	 * @return
	 */
	public void setMs(MenuServices ms) {
		this.ms = ms;
	}
	
	/**
	 *This allows to get the order services of the user 
	 * @return
	 */
	public OrderServices getOs() {
		return os;
	}
	
	/**
	 *This allows to set the order services of the user 
	 * @return
	 */
	public void setOs(OrderServices os) {
		this.os = os;
	}
	
	/**
	 *This allows to get the shopping cart services of the user 
	 * @return
	 */
	public ShoppingCartServices getScs() {
		return scs;
	}
	
	/**
	 *This allows to set the shopping cart services of the user 
	 * @return
	 */
	public void setScs(ShoppingCartServices scs) {
		this.scs = scs;
	}
	
	/**
	 *This allows to get the connection of the user 
	 * @return
	 */
	public Connection getCon() {
		return con;
	}
	
	/**
	 *This allows to set the connection of the user 
	 * @return
	 */
	public void setCon(Connection con) {
		this.con = con;
	}
	
	/**
	 *This allows to get the user name of the user 
	 * @return
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 *This allows to set the user name of the user 
	 * @return
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 *This allows to know if the user is logged in or not
	 * @return
	 */
	public boolean isLoggedIn() {
		return loggedIn;
	}
	
	/**
	 *This allows to set if the user is logged in or not
	 * @return
	 */
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	/**
	 * This prints all the info of the user instance
	 */
	@Override
	public String toString() {
		return "UserInstance [db=" + db + ", con=" + con + ", uc=" + uc + ", cs=" + cs + ", ps=" + ps + ", ms=" + ms
				+ ", os=" + os + ", scs=" + scs + ", username=" + username + ", loggedIn=" + loggedIn + "]";
	}
}
