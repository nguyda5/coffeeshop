/**
 * @author David Nguyen
 * @author Sirine Aoudj
 */
package database;
import java.sql.*;
import java.util.*;
import objects.*;
import connection.DBConnection;

public class ProductServices {
	
	private Connection con; 
	
	public ProductServices(Connection con) throws ClassNotFoundException {
		this.con = con;
	}
	
	/**
	 * This method returns all the products in the database and inserts them into java products objects with its preperties
	 * @return
	 * @throws SQLException
	 */
	public List<Product> getAllProduct() throws SQLException{
		ArrayList<Product> allProducts = new ArrayList<Product>();
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM products");
			while(rs.next()) {
				String productID = rs.getString("product_id");
				String productName = rs.getString("name");
				String productType = rs.getString("type");
				double productRetail = rs.getDouble("retail");
				int productStock = rs.getInt("stock");
				Product currentProduct = new Product(productID,productName,productType,productRetail,productStock);
				allProducts.add(currentProduct);
				System.out.println(productName + " has been added to the official menu!");
			}
		}catch(SQLException e) {
			con.rollback();
			e.printStackTrace();
			System.out.println("GETALLPRODUCTS: error");
		}
		return allProducts;
	}
	
	/**
	 * This method returns the stock of the product input, it goes in the database to find its stock 
	 * @param currentProduct
	 * @return
	 * @throws SQLException
	 */
	public int getStock(Product currentProduct) throws SQLException{
		int stock = 0;
		try {
			String SQL = "{? = call checkStock( ? )}";
			CallableStatement cstmt = con.prepareCall(SQL);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,currentProduct.getProductId());
			cstmt.executeUpdate();
			stock+=cstmt.getInt(1);
		}catch(SQLException e) {
			con.rollback();
			e.printStackTrace();
			System.out.println("GETSTOCK: error");
		}
		return stock;
	}
}
