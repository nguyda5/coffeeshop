/**
 * @author David Nguyen
 * @author Sirine Aoudj
 */
package database;

import java.sql.*;

import objects.*;

public class ShoppingCartServices {
	
	private OrderServices os;
	private Connection con; 
	
	public ShoppingCartServices(Connection con, OrderServices os) throws ClassNotFoundException {
		this.os = os;
		this.con = con;
	}
	
	/**
	 * This method creates a shopping cart for the user, into the database 
	 * then it will asign all the menu id in that shopping cart to the current shopping cart id
	 * @param sc
	 * @param username
	 * @throws SQLException
	 */
	public void checkout(ShoppingCart sc, String username) throws SQLException {
		String orderID = "0000";
		String cartID = "0000";
		os.createOrder(username);
		orderID = os.getOrderID();
		try {
			/*Create a shopping cart in the database*/
			String SQL = "{call createShoppingCart(?,?)}";
			CallableStatement cstmt = con.prepareCall(SQL);
			cstmt.setString(1,orderID);
			cstmt.registerOutParameter(2,Types.VARCHAR);
			cstmt.execute();
			cartID = cstmt.getString(2);
			con.commit();
		}catch(Exception e) {
			con.rollback();
			e.printStackTrace();
			System.out.println("CHECKOUT: error");
		}
		/*Assign the menu id to the shopping cart id in the menu shopping cart table*/
		for(int i = 0; i < sc.getAllMenus().size(); i++) {
			try {
				String SQL = "{call assignShoppingCartMenu(?,?,?)}";
				CallableStatement cstmt = con.prepareCall(SQL);
				cstmt.setString(1, cartID);
				cstmt.setString(2, sc.getAllMenus().get(i).getMenuID());
				cstmt.setInt(3, sc.getAllQuantity().get(i));
				cstmt.execute();
				System.out.println("Inserted into MENUSHOPPINGCART: " + sc.getAllMenus().get(i).getMenuID());
				con.commit();
			}catch(Exception e) {
				con.rollback();
				e.printStackTrace();
				System.out.println("CHECKOUT: error");
			}
		}
		updateStock(sc);
	}
	
	/**
	 * This method updates the stock of the database of the products that are in the shopping cart
	 * @param sc
	 * @throws SQLException
	 */
	public void updateStock(ShoppingCart sc) throws SQLException {
		for(int i = 0; i < sc.getAllMenus().size(); i++) {
			for(int a = 0; a < sc.getAllMenus().get(i).getProducts().size(); a++) {
				try {
					String SQL = "{call updateStock(?,?)";
					CallableStatement cstmt = con.prepareCall(SQL);
					String productID = sc.getAllMenus().get(i).getProducts().get(a).getProductId();
					int quantity = sc.getAllMenus().get(i).getQuantities().get(a)*sc.getAllQuantity().get(i);
					cstmt.setString(1, productID);
					cstmt.setInt(2, quantity);
					cstmt.execute();
					System.out.println("Updated stocks.");
					con.commit();
				}catch(Exception e) {
					con.rollback();
					e.printStackTrace();
					System.out.println("UPDATESTOCK: error");
				}
			}
		}	
	}	
}
