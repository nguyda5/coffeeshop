/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This home tab contains a picture that show that it is a coffee shop
 */
package client;

import database.*;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class HomeTab extends Tab{
	private UserInstance userInst;
	private Image home;
	public HomeTab(UserInstance userInst) {
		super("Home");
		this.userInst = userInst;
		this.home = new Image("images/logo.png");
		ImageView iv= new ImageView();
		iv.setImage(this.home);
		this.setContent(iv);
	}
}
