/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This class creates a shopping cart tab
 */
package client;

import java.sql.SQLException;
import java.util.*;

import database.UserInstance;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class ShoppingCartTab extends Tab{
	
	private UserInstance userInst;
	//This keeps track of the row to add to a new row when a menu is added
	int currentRow = 2;
	int currentListIndex = 0;
	int orderCreatedNum = 0;
	
	//all the shopping cart information
	private List<Label> menuIDs;
	private List<Label> quantities;
	private List<Label> items;
	private List<Label> allCost;
	private Label orderCreated = new Label("");
	private Label messageToUser = new Label("");
	private GridPane menuList = new GridPane();
	private Label menuIDLabel = new Label("Login to access shopping cart.");
	private Label quantityLabel = new Label("Quantity");
	private Label itemsLabel = new Label("Items");
	private Label menuCostLabel = new Label("Cost");
	private Button checkout = new Button("Checkout");

	/**
	 * initialize the shopping cart tab
	 * @param userInst
	 */
	public ShoppingCartTab(UserInstance userInst) {
		super("Cart");
		this.userInst = userInst;
		this.menuIDs = new ArrayList<Label>();
		this.quantities = new ArrayList<Label>();
		this.items = new ArrayList<Label>();
		this.allCost = new ArrayList<Label>();
		this.setContent(menuList);
		setupComponents();
	}
	
	/**
	 * This add all the shopping cart properties to the tab
	 */
	public void setupComponents() {
		menuList.add(messageToUser, 7, 1);
		menuList.add(checkout, 6, 1);
		menuList.add(menuIDLabel, 1,1);
		menuList.add(quantityLabel, 2,1);
		menuList.add(itemsLabel, 3,1);
		menuList.add(menuCostLabel, 4, 1);
		hideComponents();
		addCheckoutEvent();
		
		menuList.setHgap(20);
		menuList.setVgap(20);
	}
	
	/**
	 * This checks if the user if logged in or not thne it will show the components if yes
	 */
	public void checkStatus() {
		if(userInst.isLoggedIn()) {
			showComponents();
		}else {
			menuIDLabel.setText("Login to access shopping cart.");
			hideComponents();
		}
	}
	
	/**
	 * This makes the components visible
	 */
	public void showComponents() {
		messageToUser.setVisible(true);
		checkout.setVisible(true);
		menuIDLabel.setText("Menu ID");
		quantityLabel.setVisible(true);
		itemsLabel.setVisible(true);
		menuCostLabel.setVisible(true);
	}
	
	/*This makes the component invisible*/
	public void hideComponents() {
		messageToUser.setVisible(false);
		checkout.setVisible(false);
		quantityLabel.setVisible(false);
		itemsLabel.setVisible(false);
		menuCostLabel.setVisible(false);
		for(int i = 0 ; i < menuIDs.size(); i++) {
			menuIDs.get(i).setVisible(false);
			quantities.get(i).setVisible(false);
			items.get(i).setVisible(false);
			allCost.get(i).setVisible(false);
		}
	}
	
	/**
	 * This refreshes the shopping cart tab, so it will add the menus and their content and their quantity
	 */
	public void refreshCart() {
		int currentCartIndex = userInst.getCart().getAllMenus().size()-1;
		Label currentMenuID = new Label(userInst.getCart().getAllMenus().get(currentCartIndex).getMenuID());
		Label currentMenuQuantity = new Label(String.valueOf(userInst.getCart().getAllQuantity().get(currentCartIndex)));
		Label currentItems = new Label(userInst.getCart().getAllMenus().get(currentCartIndex).displayMenuItems());
		Label currentCost = new Label(userInst.getCart().getAllMenus().get(currentCartIndex).getCost() + "$");
		menuIDs.add(currentMenuID);
		quantities.add(currentMenuQuantity);
		items.add(currentItems);
		allCost.add(currentCost);
		menuList.add(menuIDs.get(currentListIndex), 1, currentRow);
		menuList.add(quantities.get(currentListIndex), 2, currentRow);
		menuList.add(items.get(currentListIndex), 3, currentRow);
		menuList.add(allCost.get(currentListIndex), 4, currentRow);
		currentRow++;
		currentListIndex++;
	}
	
	/**
	 * This makes the pane empty for the next user or order
	 */
	public void refreshPane() {
		menuList = new GridPane();
		menuList.add(messageToUser, 7, 1);
		menuList.add(checkout, 6, 1);
		menuList.add(menuIDLabel, 1,1);
		menuList.add(quantityLabel, 2,1);
		menuList.add(itemsLabel, 3,1);
		menuList.add(menuCostLabel, 4, 1);
		menuList.setHgap(20);
		menuList.setVgap(20);
		this.setContent(menuList);
		resetCounters();
	}
	
	/**
	 * This event happens when the checkout button is clicked
	 * it will create an order and update the database stock
	 */
	public void addCheckoutEvent() {
		checkout.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				if(userInst.getCart().getAllMenus().size() <= 0) {
					displayShoppingCartIsEmpty();
				}else {
					try {
						userInst.getScs().checkout(userInst.getCart(), userInst.getUsername());
						userInst.getScs().updateStock(userInst.getCart());
						userInst.resetCart();
						refreshPane();
						displaySuccessfulOrder();
						orderCreated.setText(String.valueOf(orderCreatedNum + 1));
					}catch(SQLException e1) {
						System.out.println("Error: Cannot add reference!");
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
	}
	
	/**
	 * This displays that the order has been created
	 */
	public void displaySuccessfulOrder() {
		messageToUser.setText("Order created!");
	}
	
	/**
	 * This displays that the order has not been created
	 */
	public void displayShoppingCartIsEmpty() {
		messageToUser.setText("Shopping cart is empty!");
	}
	
	/**
	 * This resets the counter when the shopping cart is emptied
	 */
	public void resetCounters() {
		currentRow = 2;
		currentListIndex = 0;
	}
	
	/**
	 * This order the order tab with the order info
	 * @return
	 */
	public Label orderUpdate() {
		return this.orderCreated;
	}
}
