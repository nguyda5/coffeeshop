/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This tab allows the user to login
 */
package client;

import java.sql.SQLException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import database.UserInstance;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class LoginTab extends Tab{
	
	/*These are the current time, the opening time and the closing time*/
	LocalTime currentTime = LocalTime.now();
	LocalTime openingTime = LocalTime.of(6,0,0);
	LocalTime closingTime = LocalTime.of(22,0,0);
	
	/*create a user instance to have access to the objects*/
	private UserInstance userInst;
	
	//User name input
	private TextField username = new TextField();
	private Label usernameLabel = new Label("Username:");
	//Password input
	private PasswordField password = new PasswordField();
	private Label passwordLabel = new Label("Password:");
	//login button
	private Button loginButton = new Button("Login");
	//Message to display if the login has been successful or not
	private Label messageToUser = new Label("");
	Button logout = new Button("Log out");
	private Label note = new Label("*Case and space sensitive");
	
	//constructor to initialize the login tab
	public LoginTab(UserInstance userInst) {
		super("Login");
		this.userInst = userInst;
		addLoginEvent();
		addLogoutEvent();
		this.setContent(addComponents());
	}

	//add all the properties to the tab
	private GridPane addComponents() {
		
		GridPane gridPane = new GridPane();
		
		this.usernameLabel.setTextFill(Color.BLACK);
		this.passwordLabel.setTextFill(Color.BLACK);
		this.loginButton.setText("Login");
		
		gridPane.add(this.usernameLabel, 1, 1);
        gridPane.add(this.username, 2, 1);
        gridPane.add(this.passwordLabel, 1, 3);
        gridPane.add(this.password, 2, 3);
        gridPane.add(this.loginButton, 2, 4);
        gridPane.add(this.messageToUser, 2, 6);
        gridPane.add(this.note, 2, 5);
        
        gridPane.setHgap(20);
        gridPane.setVgap(20);
        return gridPane;
		
	}
	
	/**
	 * This add an event to the login button
	 */
	private void addLoginEvent() {
		loginButton.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				currentTime = LocalTime.now();
				/*First it checks if the current time is between the opening hours*/
				if(currentTime.isAfter(openingTime) && currentTime.isBefore(closingTime)) {
					boolean loginSuccess = false;
					String user = username.getText();
					String currentTimeString = toCurrentTimeString(currentTime);
					
					/*then it tries to login with the user name input and the password input*/
					try {
						loginSuccess = userInst.getUc().login(user, password.getText());
					}catch(SQLException e1) {
						e1.printStackTrace();
					}
					/* it will call login success if it is true then user will be logged in*/
					if(loginSuccess) {
						userInst.setUsername(user);
						userInst.setLoggedIn(true);
						addUserLoggedIn(user);
						try {
							userInst.getUc().insertLoginTime(userInst.getUsername(), currentTimeString);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}else {
						/*If the login success is false then it will display that one of the fields is invalid*/
						messageToUser.setText("Invalid username/password");
					}
				}else {
					/*If the current time is not in the valid hours then it will display to come back in the opening hours*/
					messageToUser.setText("Come back between 6 AM and 10 PM!");
				}
			}
		});
	}
	/*
	 * converts the time given into a string
	 */
	public String toCurrentTimeString(LocalTime time) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		String timeString = time.format(formatter);
		return timeString;
	}
	
	/**
	 *This displays that when the user is logged in, there will be a logout button to logout 
	 * @param user
	 */
	private void addUserLoggedIn(String user) {
		GridPane userLoggedIn = new GridPane();
		Label loggedInAs = new Label("Logged in as: " + user);
		userLoggedIn.add(logout, 1, 1, 1, 1);
		userLoggedIn.add(loggedInAs, 2, 1, 1, 1);
		userLoggedIn.setHgap(20);
	    userLoggedIn.setVgap(20);
	    this.loginButton.setText("logout");
		this.setContent(userLoggedIn);
	}
	
	/**
	 * This will display when the user clicks on the logout button
	 */
	public void addLogoutEvent() {
		logout.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				userInst.setUsername("");
				userInst.setLoggedIn(false);
				messageToUser.setText("Logged Out");
				resetLoginButton();
				resetForm();
				username.setText("");
				password.setText("");
				
			}
		});
	}
	
	/**
	 * This will reset the form when the user logs out
	 */
	public void resetForm() {
		this.setContent(addComponents());
	}
	
	/**
	 * This reset the button to login when the user logs out
	 */
	public void resetLoginButton() {
		this.loginButton.setText("Login");
	}
	
	/**
	 * This resets the button to logout when the user logged in
	 * @return
	 */
	public Button getLoginButton() {
		return this.loginButton;
	}
}
