/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This class creates a order tab that allows to contains the order date, delivery date and order id
 */
package client;

import java.sql.SQLException;

import database.UserInstance;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;

public class OrderTab extends Tab{
	private UserInstance userInst;
	private GridPane form;
	private Label orderLabel = new Label("Your Order: ");
	private Label orderDateLabel = new Label("Order Date: ");
	private Label deliveryLabel = new Label("Delivery Date: ");
	
	private Label orderId = new Label("");
	private Label orderDate = new Label("");
	private Label deliveryDate = new Label("");
	
	private Label messageToUser = new Label("");
	
	//initialize and order tab
	public OrderTab(UserInstance userInst) {
		super("Order");
		this.userInst = userInst;
		this.form = new GridPane();
		addComponents();
		this.setContent(form);
		setInvisible();
	}

	//add the properties of the order tab to the order tab
	public void addComponents() {
		 form.add(orderLabel, 1, 1);
		 form.add(orderDateLabel, 1, 2);
		 form.add(deliveryLabel, 1, 3);
		 form.add(orderId, 2, 1);
		 form.add(orderDate, 2, 2);
		 form.add(deliveryDate, 2, 3);
		 form.setHgap(20);
		 form.setVgap(20);
	}
	
	/**
	 * This refreshes the order tab when an order has been created
	 * @throws SQLException
	 */
	public void refreshOrder() throws SQLException {
		orderId.setText(userInst.getOs().getOrderID());
		orderDate.setText(userInst.getOs().getOrderDate());
		deliveryDate.setText(userInst.getOs().getDeliveryDate());
	}
	
	/**
	 * This set the fields to visible when the user is logged in
	 */
	public void setVisible() {
		orderLabel.setText("Your Order: ");
		orderDateLabel.setVisible(true);
		deliveryLabel.setVisible(true);
	}
	
	/**
	 * This set the fields to invisible when the user is logged out
	 */
	public void setInvisible() {
		orderLabel.setText("Login in order to make an order!");
		orderDateLabel.setVisible(false);
		deliveryLabel.setVisible(false);
	}
	
	/**
	 * This check if the user is logged in or not
	 */
	public void checkStatus() {
		if(userInst.isLoggedIn()) {
			setVisible();
		}else {
			setInvisible();
		}
	}

}
