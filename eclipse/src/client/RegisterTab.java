/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This class creates a register tab that allows to create a customer/user
 */
package client;

import java.sql.SQLException;

import database.UserInstance;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import objects.*;

public class RegisterTab extends Tab{
	private UserInstance userInst;
	private GridPane form;
	//all the field to create a customer/user
	TextField usernameField = new TextField();
	PasswordField passwordField = new PasswordField();
	PasswordField reenterPasswordField = new PasswordField();
	TextField deliveryAddressField = new TextField();
	TextField phoneNumberField = new TextField();
	TextField emailField = new TextField();
	TextField referenceUserField = new TextField();
	Button registerButton = new Button("Register");
	Label messageToUser = new Label("");
	
	/**
	 * Initialize the register tab
	 * @param userInst
	 */
	public RegisterTab(UserInstance userInst) {
		super("Register");
		this.userInst = userInst;
		this.form = new GridPane();
		addComponents();
		this.setContent(form);
	}
	/**
	 * add the content of the register tab
	 */
	public void addComponents() {
		addRegisterUsername();
		addRegisterPassword();
		addRegisterReenterPassword();
		addRegisterDeliveryAddress();
		addRegisterPhoneNumber();
		addRegisterEmail();
		addRegisterReferenceUser();
		addRegisterButton();
		addRegisterEvent();
		addMessageToUser();
		form.setHgap(20);
		form.setVgap(20);
		
	}
	
	/**
	 * add the username field
	 */
	public void addRegisterUsername() {
		Label usernameLabel = new Label("Username:");
		form.add(usernameLabel, 1, 1, 1, 1);
		form.add(usernameField, 2, 1, 1, 1);
	}
	
	/**
	 * add the password field
	 */
	public void addRegisterPassword() {
		Label passwordLabel = new Label("Password:");
		form.add(passwordLabel, 1, 2, 1, 1);
		form.add(passwordField, 2, 2, 1, 1);
	}
	
	/**
	 * add the confirm password field
	 */
	public void addRegisterReenterPassword() {
		Label reenterPasswordLabel = new Label("Re-enter Password:");
		form.add(reenterPasswordLabel, 1, 3, 1, 1);
		form.add(reenterPasswordField, 2, 3, 1, 1);
	}
	
	/**
	 * add the address field
	 */
	public void addRegisterDeliveryAddress() {
		Label deliveryAddressLabel = new Label("Delivery Address:");
		form.add(deliveryAddressLabel, 1, 4, 1, 1);
		form.add(deliveryAddressField, 2, 4, 1, 1);
	}
	
	/**
	 * add the phone number field
	 */
	public void addRegisterPhoneNumber() {
		Label phoneNumberLabel = new Label ("Phone Number:");
		form.add(phoneNumberLabel, 1, 5, 1, 1);
		form.add(phoneNumberField, 2, 5, 1 ,1);
	}
	
	/**
	 * add the email field
	 */
	public void addRegisterEmail() {
		Label emailLabel = new Label ("Email:");
		form.add(emailLabel, 1, 6, 1 , 1);
		form.add(emailField, 2, 6, 1 , 1);
	}
	
	/**
	 * add the reference user field
	 */
	public void addRegisterReferenceUser() {
		Label referenceUserLabel = new Label("Referred by:");
		form.add(referenceUserLabel, 1, 7, 1, 1);
		form.add(referenceUserField, 2, 7, 1, 1);
	}
	
	/**
	 * add the register button
	 */
	public void addRegisterButton() {
		form.add(registerButton, 2, 8, 1 ,1);
	}
	
	/**
	 * add the message to user
	 */
	public void addMessageToUser() {
		form.add(messageToUser, 1, 9, 1, 1);
	}
	
	/**
	 * add the register event when the register button is clicked
	 * it will create a customer into the database if it does not already exists and all the fields are filled
	 */
	public void addRegisterEvent() {
		registerButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e){
				if(checkPassword() && checkFields()) {
					String user = usernameField.getText();
					String password = passwordField.getText();
					String deliveryAddress = deliveryAddressField.getText();
					String phoneNumber = phoneNumberField.getText();
					String email = emailField.getText();
					String referred = referenceUserField.getText();
					System.out.println(password);
					Customer currentCustomer = new Customer(deliveryAddress, phoneNumber, email, referred);
					try {
						userInst.getUc().newUser(user,password, messageToUser);
						userInst.getCs().addCustomer(user, currentCustomer, messageToUser);
						userInst.getCs().addReference(user, currentCustomer);
						//messageToUser.setText("User has been created!");
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					//reset the fields to empty to create another user
					resetField();
				}
			}
		});
	}
	
	/**
	 * this check if the re enter password matches to the password input
	 * @return
	 */
	public boolean checkPassword() {
		String pass = passwordField.getText();
		String passToCheck = reenterPasswordField.getText();
		if(pass.equals(passToCheck)) {
			return true;
		}
		messageToUser.setText("Password does not match!");
		return false;
	}
	
	/**
	 * this resets the inputs to empty
	 */
	public void resetField() {
		usernameField.setText("");
		passwordField.setText("");
		reenterPasswordField.setText("");
		deliveryAddressField.setText("");
		phoneNumberField.setText("");
		emailField.setText("");
		referenceUserField.setText("");
	}
	
	/**
	 * This check if all the fields are filled out 
	 * @return
	 */
	public boolean checkFields() {
		boolean containFields = true;
		if(usernameField.getText().trim().isEmpty()||
			passwordField.getText().isEmpty()||
			deliveryAddressField.getText().trim().isEmpty()||
			phoneNumberField.getText().trim().isEmpty()||
			emailField.getText().trim().isEmpty()) {
			containFields = false;
			messageToUser.setText("Incomplete info");
		}
		return containFields;
	}
}
