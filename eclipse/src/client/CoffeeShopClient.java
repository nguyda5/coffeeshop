/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This class creates an application of the coffee shop
 */
package client;

import java.sql.SQLException;

import database.*;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class CoffeeShopClient extends Application{
	
	/**
	 * start sets up the scene
	 * @param Stage stage
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public void start(Stage stage) throws ClassNotFoundException, SQLException {
		UserInstance userInst = new UserInstance();
		
		//set the app height and width
		final int WIDTH = 800;
		final int HEIGHT = 700;
		Group root = new Group();
		
		//create a scene
		Scene scene = new Scene(root,WIDTH,HEIGHT);
		scene.setFill(Color.BISQUE);
		System.out.println(userInst.toString());
		//initialize all the tabs
		TabPane tb = new TabPane();
		HomeTab homeTab = new HomeTab(userInst);
		RegisterTab registerTab = new RegisterTab(userInst);
		LoginTab loginTab = new LoginTab(userInst);
		MenuTab menuTab = new MenuTab(userInst);
		ShoppingCartTab cartTab = new ShoppingCartTab(userInst);
		OrderTab orderTab = new OrderTab(userInst);
		
		/*This method check if the button is in logged in state it allows the properties to have access
		  to the coffee shop to be enabled*/
		loginTab.getLoginButton().textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				try {
					menuTab.checkStatus();
					cartTab.checkStatus();
					orderTab.checkStatus();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		
		/*This check if a menu has been created, it will add it to the shopping cart*/
		menuTab.getUpdateCart().textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				cartTab.refreshCart();
			}
		});
		
		/*This checks if the orderButton has been clicked and will display the new order in the order tab*/
		cartTab.orderUpdate().textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				try {
					orderTab.refreshOrder();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		
		//add the tabs to the scene
		tb.getTabs().add(homeTab);
		tb.getTabs().add(registerTab);
		tb.getTabs().add(loginTab);
		tb.getTabs().add(menuTab);
		tb.getTabs().add(cartTab);
		tb.getTabs().add(orderTab);
		
		root.getChildren().add(tb);
		
		//start the scene
		stage.setScene(scene);
		stage.show();

	}
	/**
	 * main launches the application
	 * @param args
	 */
	public static void main(String[]args) {
		//Launches the application
		Application.launch(args);
	}
}
