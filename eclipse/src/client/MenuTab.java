/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This Tab allows to creates menus and add them to the shopping cart
 */
package client;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.UserInstance;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import objects.Product;

public class MenuTab extends Tab{
	
	//This variable keeps count of the menu added to the shopping cart
	//It allows to update the shopping cart
	int cartUpdateCount = 0;
	private UserInstance userInst;
	private GridPane form;
	
	//These contains the images of the items we have on our menu
	//David drew all of these items pictures :), so no external source
	private Image espressoLogo = new Image("images/espresso.png");
	private Image latteLogo = new Image("images/latte.png");
	private Image regularLogo = new Image("images/regular.png");
	private Image icedLogo = new Image("images/ice.png");
	private Image milkLogo = new Image("images/milk.png");
	private Image sugarLogo = new Image("images/sugar.png");
	private Image creamLogo = new Image("images/cream.png");
	private Image muffinLogo = new Image("images/muffin.png");
	private Image cookieLogo = new Image("images/cookie.png");
	private Image cupcakeLogo = new Image("images/cupcake.png");
	
	//These are the products names and their prices
	private	Text espresso = new Text("Espresso, 2.25$");
	private	Text latte = new Text("Latte, 2,75 $");
	private	Text regular = new Text("Regular, 2,00$");
	private	Text iced = new Text("Iced, 2,50$");
	private	Text milk = new Text("Milk, Free with a coffee");
	private	Text sugar = new Text("Sugar, Free with a coffee");
	private	Text cream = new Text("Cream, 0,20$");
	private	Text muffin = new Text("Muffin, 4,00$");
	private	Text cookie = new Text("Cookie, 2,00$");
	private	Text cupcake = new Text("Cupcake, 2,00$");
	
	//these are the fields to input the quantity of each item to a menu
	private TextField espressoQt = new TextField();
	private TextField latteQt = new TextField();
	private TextField regularQt = new TextField();
	private TextField icedQt = new TextField();
	private TextField milkQt = new TextField();
	private TextField sugarQt = new TextField();
	private TextField creamQt = new TextField();
	private TextField muffinQt = new TextField();
	private TextField cookieQt = new TextField();
	private TextField cupcakeQt = new TextField();
	
	//These are the buttons of each item, to add the item
	private Button addEspresso = new Button("Add");
	private Button addLatte = new Button("Add");
	private Button addRegular = new Button("Add");
	private Button addIced = new Button("Add");
	private Button addMilk = new Button("Add");
	private Button addSugar = new Button("Add");
	private Button addCream = new Button("Add");
	private Button addMuffin = new Button("Add");
	private Button addCookie = new Button("Add");
	private Button addCupcake = new Button("Add");
	
	//These are the labels to display current information of that user
	private Label quantityLabel = new Label("Quantity");
	private Label menuCostLabel = new Label("Cost: ");
	private Label menuCost = new Label("0.00$");
	private Button addMenu = new Button("Create Menu!");
	private Label messageToUser = new Label("");
	private Button applyDiscount = new Button("Apply Discount");
	private Label discountsAvailable = new Label("Discounts Available: 0");
	private Label discountNote = new Label("*Refer people to get 5$ discounts, max 3 reference!");
	private Label menuQtLabel = new Label("Menu quantity: ");
	private Label menuQt = new Label("1");
	private Button addMenuQt = new Button("Add");
	private Label updateCart = new Label("");
	
	//list of all the products in a menu
	private List<Product> allProducts = new ArrayList<Product>();
	
	/**
	 * Initialize the menu tab
	 * @param userInst
	 * @throws SQLException
	 */
	public MenuTab(UserInstance userInst) throws SQLException {
		super("Menu");
		this.userInst = userInst;
		allProducts = userInst.getPs().getAllProduct();
		this.form = new GridPane();
		addComponents();
		this.setContent(form);
	}
	
	/**
	 * Add all the menu tab properties to the menu tab aand add the events to the buttons
	 * @throws SQLException
	 */
	public void addComponents() throws SQLException {
		addImages();
		addItemsNames();
		addButtons();
		addQuantities();
		convertAllTextField();
		addProductEventToAllButtons();
		checkStatus();
		addDiscountUpdate();
		addMenuQtEvent();
		addMenuEvent();
		
		form.add(menuQtLabel, 2, 11 );
		form.add(menuQt, 3, 11);
		form.add(addMenuQt, 4, 11);
		form.add(discountNote, 5, 12);
		form.add(discountsAvailable, 5,13);
		form.add(applyDiscount, 4,13);
		form.add(messageToUser, 2, 13);
		form.add(quantityLabel,4 , 0);
		form.add(menuCostLabel, 2, 12);
		form.add(menuCost, 3, 12);
		form.add(addMenu, 4, 12);
		
		form.setHgap(20);
		form.setVgap(20);
	}
	
	/**
	 * This makes a image into a image view to be able to add it to the grid pane
	 * @param img
	 * @return
	 */
	public ImageView makeView(Image img) {
		ImageView iv = new ImageView();
		iv.setImage(img);
		return iv;
	}
	
	/**
	 * This checks if the user is logged in or not
	 * @throws SQLException
	 */
	public void checkStatus() throws SQLException {
		if(userInst.isLoggedIn()) {
			loggedIn();
		}else {
			loggedOut();
		}
	}
	
	/**
	 * This displays the buttons and if the user is logged in
	 * @throws SQLException
	 */
	public void loggedIn() throws SQLException {
		enableButtons();
		enableQuantities();
		enableOthers();
		checkDiscounts();
	}
	
	/**
	 * this disables the buttons if the user is logged out
	 */
	public void loggedOut() {
		disableButtons();
		disableQuantities();
		disableOthers();
	}
	
	/**
	 * This add the images to the menu tab
	 */
	public void addImages() {
		form.add(makeView(espressoLogo), 1, 1);
		form.add(makeView(latteLogo), 1, 2);
		form.add(makeView(regularLogo), 1, 3);
		form.add(makeView(icedLogo), 1, 4);
		form.add(makeView(milkLogo), 1, 5);
		form.add(makeView(sugarLogo), 1, 6);
		form.add(makeView(creamLogo), 1, 7);
		form.add(makeView(muffinLogo), 1, 8);
		form.add(makeView(cookieLogo), 1, 9);
		form.add(makeView(cupcakeLogo), 1, 10);
	}
	
	/**
	 * This adds the items names to the menu tab
	 */
	public void addItemsNames() {
		form.add(espresso, 2, 1);
		form.add(latte, 2, 2);
		form.add(regular, 2, 3);
		form.add(iced, 2, 4);
		form.add(milk, 2, 5);
		form.add(sugar, 2, 6);
		form.add(cream, 2, 7);
		form.add(muffin, 2, 8);
		form.add(cookie, 2, 9);
		form.add(cupcake, 2, 10);
	}
	
	/**
	 * This adds the items buttons to the menu tab
	 */
	public void addButtons() {
		form.add(addEspresso, 3, 1);
		form.add(addLatte, 3, 2);
		form.add(addRegular, 3, 3);
		form.add(addIced, 3, 4);
		form.add(addMilk, 3, 5);
		form.add(addSugar, 3, 6);
		form.add(addCream, 3, 7);
		form.add(addMuffin, 3, 8);
		form.add(addCookie, 3, 9);
		form.add(addCupcake, 3, 10);
	}
	
	/**
	 * This adds the quantity inputs to the menu tab
	 */
	public void addQuantities() {
		form.add(espressoQt, 4, 1);
		form.add(latteQt, 4, 2);
		form.add(regularQt, 4, 3);
		form.add(icedQt, 4, 4);
		form.add(milkQt, 4, 5);
		form.add(sugarQt, 4, 6);
		form.add(creamQt, 4, 7);
		form.add(muffinQt, 4, 8);
		form.add(cookieQt, 4, 9);
		form.add(cupcakeQt, 4, 10);
		
	}
	
	/**
	 * This disables the buttons of the menu tab
	 */
	public void disableButtons() {
		addEspresso.setVisible(false);
		addLatte.setVisible(false);
		addRegular.setVisible(false);
		addIced.setVisible(false);
		addMilk.setVisible(false);
		addSugar.setVisible(false);
		addCream.setVisible(false);
		addMuffin.setVisible(false);
		addCookie.setVisible(false);
		addCupcake.setVisible(false);
	}
	
	/**
	 * This enables the buttons of the menu tab
	 */
	public void enableButtons() {
		addEspresso.setVisible(true);
		addLatte.setVisible(true);
		addRegular.setVisible(true);
		addIced.setVisible(true);
		addMilk.setVisible(true);
		addSugar.setVisible(true);
		addCream.setVisible(true);
		addMuffin.setVisible(true);
		addCookie.setVisible(true);
		addCupcake.setVisible(true);
	}
	
	/**
	 * This enables the quantity inputs of the menu tab
	 */
	public void enableQuantities() {
		espressoQt.setVisible(true);
		latteQt.setVisible(true);
		regularQt.setVisible(true);
		icedQt.setVisible(true);
		milkQt.setVisible(true);
		sugarQt.setVisible(true);
		creamQt.setVisible(true);
		muffinQt.setVisible(true);
		cookieQt.setVisible(true);
		cupcakeQt.setVisible(true);
	}
	
	/**
	 * This disables the quantity inputs of the menu tab
	 */
	public void disableQuantities() {
		espressoQt.setVisible(false);
		latteQt.setVisible(false);
		regularQt.setVisible(false);
		icedQt.setVisible(false);
		milkQt.setVisible(false);
		sugarQt.setVisible(false);
		creamQt.setVisible(false);
		muffinQt.setVisible(false);
		cookieQt.setVisible(false);
		cupcakeQt.setVisible(false);
	}
	
	/**
	 * This enables the other properties of the menu tab
	 */
	public void enableOthers() {
		quantityLabel.setVisible(true);
		menuCostLabel.setVisible(true);
		menuCost.setVisible(true);
		addMenu.setVisible(true);
		messageToUser.setVisible(true);
		applyDiscount.setVisible(true);
		discountsAvailable.setVisible(true);
		discountNote.setVisible(true);
		menuQtLabel.setVisible(true);
		menuQt.setVisible(true);
		addMenuQt.setVisible(true);
	}
	
	/**
	 * This disables the other properties of the menu tab
	 */
	public void disableOthers() {
		quantityLabel.setVisible(false);
		menuCostLabel.setVisible(false);
		menuCost.setVisible(false);
		addMenu.setVisible(false);
		messageToUser.setVisible(false);
		applyDiscount.setVisible(false);
		discountsAvailable.setVisible(false);
		discountNote.setVisible(false);
		menuQtLabel.setVisible(false);
		menuQt.setVisible(false);
		addMenuQt.setVisible(false);
	}
	
	/**
	 * This creates changes the menu quantity property when the create menu is clicked on to keep track
	 * of th e amount of menu created, to update the shopping cart
	 */
	public void addMenuQtEvent() {
		addMenuQt.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				int count = Integer.parseInt(menuQt.getText());
				count+=1;
				menuQt.setText(String.valueOf(count));
			}
		});
	}
	
	/**
	 * This adds a product to the menu with its quantity when add button is clicked 
	 * and displays what product has been added and its quantity
	 * it also updates the cost of the menu in the tab
	 * @param currentButton
	 * @param quantityField
	 * @param product
	 */
	public void addProductEvent(Button currentButton,TextField quantityField, Product product) {
		currentButton.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				int quantity = Integer.parseInt(quantityField.getText());
				try {
					userInst.getMenu().addProduct(product, quantity, messageToUser);
					displayCost();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * This make sit so that an text field can only input a number
	 * @param currentTextField
	 */
	public void numberOnly(TextField currentTextField) {
		currentTextField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				if (!arg2.matches("\\d*")) {
		            currentTextField.setText(arg2.replaceAll("[^\\d]", ""));
		        }
			}
		});
	}
	
	/**
	 * This adds a discount to the menu when the add discount button is clicked
	 * first it checks if the user can apply a discount or not
	 * then if it can it will apply the discount, if not it will display that this cannot be done
	 */
	public void addDiscountUpdate() {
		applyDiscount.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				if(!userInst.getMenu().getDiscountApplied()) {
					try {
						if(userInst.getCs().getDiscounts(userInst.getUsername()) > 0) {
							if(userInst.getMenu().getCost() - 5 < 0) {
								messageToUser.setText("Menu needs to be at least 5$");
							}else {
								userInst.getMenu().setCost(userInst.getMenu().getCost() - 5);
								userInst.getMenu().setDiscountApplied(true);
								displayCost();
								messageToUser.setText("Discount applied!");
							}
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else {
					messageToUser.setText("Already applied discount!");
				}
			}
		});
	}
	
	/**
	 * This method sets an event when the create menu button is clicked
	 */
	public void addMenuEvent() {
		addMenu.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent e) {
				//First it check if the menu has items
				if(userInst.getMenu().getProducts().size() > 0) {
					//then it checks if a discount has been applied
					if(userInst.getMenu().getDiscountApplied()) {
						try {
							userInst.getMenu().useDiscount(userInst.getUsername(), 1);
							System.out.println("Discount updated in DB for: " + userInst.getUsername());
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
					}
					//add menu to shopping cart and db
					int qt = Integer.parseInt(menuQt.getText());
					try {
						userInst.getCart().addMenu(userInst.getMenu(),qt, userInst.getCon());
						userInst.getMs().addMenu(userInst.getUsername(), userInst.getMenu());//adds to db
						userInst.resetMenu();
						clearFields();
						checkDiscounts();
						updateCart.setText(String.valueOf(cartUpdateCount+=1));
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else {
					//if menu is empty it will display this message
					messageToUser.setText("Your menu is empty!");
				}
			}
		});
	}
	
	/**
	 * This updates the shopping cart
	 * @return
	 */
	public Label getUpdateCart() {
		return this.updateCart;
	}
	
	/**
	 * This makes the quantity fields clear when a menu is created
	 */
	public void clearFields() {
		espressoQt.setText("");
		latteQt.setText("");
		regularQt.setText("");
		icedQt.setText("");
		milkQt.setText("");
		sugarQt.setText("");
		creamQt.setText("");
		muffinQt.setText("");
		cookieQt.setText("");
		cupcakeQt.setText("");
		menuCost.setText("0.00$");
		menuQt.setText("1");
		
	}
	
	/**
	 * This makes it that all the quantity fields can only input numbers
	 */
	public void convertAllTextField() {
		numberOnly(espressoQt);
		numberOnly(latteQt);
		numberOnly(regularQt);
		numberOnly(icedQt);
		numberOnly(milkQt);
		numberOnly(sugarQt);
		numberOnly(creamQt);
		numberOnly(muffinQt);
		numberOnly(cookieQt);
		numberOnly(cupcakeQt);
	}
	
	
	/**
	 * add the addproduct events to all the add buttons
	 */
	public void addProductEventToAllButtons() {
		addProductEvent(addEspresso, espressoQt, allProducts.get(0));
		addProductEvent(addLatte, latteQt, allProducts.get(1));
		addProductEvent(addRegular, regularQt, allProducts.get(2));
		addProductEvent(addIced, icedQt, allProducts.get(3));
		addProductEvent(addMilk, milkQt, allProducts.get(4));
		addProductEvent(addSugar, sugarQt, allProducts.get(5));
		addProductEvent(addCream, creamQt, allProducts.get(6));
		addProductEvent(addMuffin, muffinQt, allProducts.get(7));
		addProductEvent(addCookie, cookieQt, allProducts.get(8));
		addProductEvent(addCupcake, cupcakeQt, allProducts.get(9));
	}
	
	/**
	 * this displays the cost of the current menu
	 */
	public void displayCost() {
		String cost = String.valueOf(userInst.getMenu().getCost()) + "$";
		menuCost.setText(cost);
	}
	
	/**
	 * This check if th euser has any discounts available
	 * @throws SQLException
	 */
	public void checkDiscounts() throws SQLException {
		discountsAvailable.setText("Discounts Available: " + userInst.getCs().getDiscounts(userInst.getUsername()));
	}

}
