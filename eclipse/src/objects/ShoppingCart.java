/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This class contains all the information of a shopping cart
 */
package objects;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import database.*;

public class ShoppingCart {
	//All the menu objects in that shopping cart
	private List<Menu> allMenus;
	//The quantity of these menus
	private List<Integer> allQuantity;
	//The cost of that shopping cart
	private double totalCost;
	
	/**
	 * 	This creates a shopping cart
	 */
	public ShoppingCart() {
		this.allMenus = new ArrayList<Menu>();
		this.allQuantity = new ArrayList<Integer>();
		this.totalCost = 0;
	}
	
	/**
	 * This creates a shopping cart but with a list of menu,quantities and their cost
	 * @param allMenus
	 * @param allQuantity
	 * @param totalCost
	 */
	public ShoppingCart(List<Menu> allMenus, List<Integer> allQuantity, double totalCost) {
		super();
		this.allMenus = allMenus;
		this.allQuantity = allQuantity;
		this.totalCost = totalCost;
	}
	
	/**
	 * This displays the total cost of that shopping cart 
	 */
	public void displayTotalCost() {
		System.out.println("Shopping cart cost: " + totalCost + "$");
	}
	
	
	/**
	 * This adds a menu to the shopping cart with its quantity
	 * @param currentMenu
	 * @param qt
	 * @param con
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void addMenu(Menu currentMenu, int qt, Connection con) throws SQLException, ClassNotFoundException {
		ProductServices ps = new ProductServices(con);
		boolean cantAdd = false;
		/*First it checks if the stock has the quantity that the menu contains
		 If yes then cantAdd will stay false, else it will be set to true*/ 
		for(int i = 0; i < currentMenu.getProducts().size(); i++) {
			int quantity = 0;
			quantity+=currentMenu.getQuantity(currentMenu.getProducts().get(i))*qt;
			int stock = 0;
			stock = ps.getStock(currentMenu.getProducts().get(i));
			if(stock - quantity < 0) {
				System.out.println("Menu quantity exceeds stock!");
				cantAdd = true;
			}
			
		}
		/*Then it check if a discount is applied, if yes then it will not allow the user to get multiples of that menu*/
		if(currentMenu.getDiscountApplied() && qt > 1) {
			System.out.println("Discount applied to menu! Can't get multiple of this menu.");
			cantAdd = true;
		}
		/*If the cantAdd is false then the menu multiplied by its quantity can be added
		  it will then be added to the list of menus and the quantity to the list of quantity*/
		if(!cantAdd) {
			System.out.println("Added menu to shopping cart");
			this.allMenus.add(currentMenu);
			this.allQuantity.add(qt);
			totalCost+=qt*currentMenu.getCost();
		}

	}
	
	/**
	 * This returns the list of menus
	 * @return
	 */
	public List<Menu> getAllMenus() {
		return allMenus;
	}
	
	/**
	 * This allows to set the list of menu in that shopping cart
	 * @param allMenus
	 */
	public void setAllMenus(List<Menu> allMenus) {
		this.allMenus = allMenus;
	}
	
	/**
	 * This gets the list of quantities of the menus in that shopping cart
	 * @return
	 */
	public List<Integer> getAllQuantity() {
		return allQuantity;
	}
	
	/**
	 * This allows to set the quantity of that shopping cart
	 * @param allQuantity
	 */
	public void setAllQuantity(List<Integer> allQuantity) {
		this.allQuantity = allQuantity;
	}
	
	/**
	 * This allows to get the total cost of that shopping cart
	 * @return
	 */
	public double getTotalCost() {
		return totalCost;
	}
	
	/**
	 * This allows to set the total cost of the shooping cart
	 * @param totalCost
	 */
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	
}
