/**
 * @author David Nguyen
 * @author Sirine Aoudj
 * This class contains all the information of a menu
 */
package objects;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import objects.*;

import connection.DBConnection;
import javafx.scene.control.Label;

public class Menu {
	private String menuID = "0000";
	
	//That products list object contains all the names of the products in that menu
	private List <Product> products = new ArrayList<Product>();  
	//That quantities list object contains all the quantities of each product in that menu
	private List <Integer> quantities = new ArrayList<Integer>();
	//This variable keeps track of whether the customer applied a discount on that menu
	private boolean discountApplied;
	//This stores the total price of that menu
	private double cost;
	private DBConnection db;
	private Connection con;
	
	/**
	 * This returns true if a discount has been applied on that menu
	 * @return
	 */
	public boolean getDiscountApplied() {
		return this.discountApplied;
	}
	
	/**
	 * This allows us to get the menu id of that menu
	 * @return
	 */
	public String getMenuID() {
		return this.menuID;
	}
	
	/**
	 * This sets the menu id of that menu
	 * @param menuID is the id we want to set
	 */
	public void addMenuID(String menuID) {
		this.menuID = menuID;
	}
	
	/**
	 * This allows us to get all the products names of that menu
	 * @return
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * This allows to set a list of product names for that menu
	 * @param products
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	/**
	 * This method allows us to get all the quantities of the products of that menu
	 * @return
	 */
	public List<Integer> getQuantities() {
		return quantities;
	}

	/**
	 * This method allows us to set the quantitites of the products of that menu
	 * @param quantities
	 */
	public void setQuantities(List<Integer> quantities) {
		this.quantities = quantities;
	}

	/**
	 * This allows to get the total cost of that menu
	 * @return
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * This allows to set the cost of that menu
	 * @param cost
	 */
	public void setCost(double cost) {
		this.cost = cost;
	}

	/**
	 * get the db of that menu
	 * @return
	 */
	public DBConnection getDb() {
		return db;
	}

	/**
	 * set the connection of that menu
	 * @param db
	 */
	public void setDb(DBConnection db) {
		this.db = db;
	}

	/**
	 * This gets the connection of that menu
	 * @return
	 */
	public Connection getCon() {
		return con;
	}

	/**
	 * This sets the connection of that menu
	 * @param con
	 */
	public void setCon(Connection con) {
		this.con = con;
	}

	/**
	 * This constructor allows to create a menu 
	 * @throws ClassNotFoundException
	 */
	public Menu() throws ClassNotFoundException{
		db = new DBConnection();
		con = db.getConnection();
		cost = 0;
		discountApplied = false;
	}
	
	/**
	 * This returns the quantity of the product that we input
	 * @param product is the product we want to get the quantity of
	 * @return int that is the quantity of the product
	 * @throws SQLException
	 */
	public int getQuantity(Product product) throws SQLException {
		int quantity = 0;
		for(int i = 0; i < quantities.size(); i++) {
			if(products.get(i).getName().equals(product.getName())) {
				quantity+=quantities.get(i);
			}
		}
		return quantity;
	}
	
	/**
	 * This method checks the stocks of the product
	 * It will return true if there is enough stock to add the product to the menu
	 * it will return false if the quantity input is too high for the stock
	 * @param product the product we want to add to the menu
	 * @param quantity is the amount of that product that we want to add to the menu
	 * @return true or false depending on the stock and quantity
	 * @throws SQLException
	 */
	public Boolean checkStock(Product product, int quantity) throws SQLException {
		 if(this.quantities.size() > 0) {
			 for(int i = 0; i < quantities.size(); i++ ) {
				 if(products.get(i).getName().equals(product.getName())) {
					 System.out.println("added existing qt");
					 quantity+=quantities.get(i);
				 }
			 }
		 }
		CallableStatement cstmt = 
		con.prepareCall("{? = call checkStock( ? )}");
		cstmt.registerOutParameter(1, Types.INTEGER);
		cstmt.setString(2, product.getProductId());
		cstmt.executeUpdate();
		
		int stock = cstmt.getInt(1);
		
		if(stock-quantity >= 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * This methods check is the user has a coffee in their current menu
	 * This allows us to know if the person can add a toping to their menu
	 * @param products is the list of products in the current menu
	 * @return string true or false depending if the menu contains a coffee or not
	 * @throws SQLException
	 */
	 public String checkCoffee(List <Product> products) throws SQLException {
		String coffee = "coffee";
		String containsCoffee = "false";
		
		for(int i =0; i < products.size(); i++) {
			
			//Call the checkType function that verifies the type of all the products to see if it find one that is coffee
			Product currProduct = products.get(i);
			CallableStatement cstmt = 
			con.prepareCall("{? = call checkType( ?, ? )}");
			cstmt.registerOutParameter(1, Types.VARCHAR);
			cstmt.setString(2, currProduct.getProductId());
			cstmt.setString(3, coffee);
			cstmt.executeUpdate();
			
			containsCoffee = cstmt.getString(1);
			
			if(containsCoffee.equals("true")) {
				return containsCoffee;
				}
			}
		return containsCoffee;
		}
	 
	 /**
	  * This method check if the type of the product input is of type topping
	  * this method will be used to check if a coffee is needed in the menu
	  * @param product
	  * @return
	  * @throws SQLException
	  */
	 public String checkTopping(Product product) throws SQLException {
		 String type = "topping";
		 String isTopping = "false";
		 
		 CallableStatement cstmt = 
				con.prepareCall("{? = call checkType( ?, ? )}");
				cstmt.registerOutParameter(1, Types.VARCHAR);
				cstmt.setString(2, product.getProductId());
				cstmt.setString(3, type);
				cstmt.executeUpdate();
				
				isTopping = cstmt.getString(1);
				
				return isTopping;
	 }
	 
	 /**
	  * This method adds a product to the menu with its quantity
	  * @param product
	  * @param quantity
	  * @param messageToUser
	  * @throws SQLException
	  */
	 public void addProduct(Product product, int quantity, Label messageToUser) throws SQLException {
		
		 //This calculates the price of the product multiplied with it's quantity
		 double price = product.getRetail()*quantity;
		 
		 //First it check if the quantity is available
		 if(checkStock(product,quantity)) {
			 //If it is available, it check if it is a toping
			 if(checkTopping(product).equals("true")) {
				 //If it is a toping it checks if the menu contains an item of type coffee
				 if(this.checkCoffee(this.products).equals("true")) {
					 //If it contains a coffee it will add it and display the item added to the menu
					 this.products.add(product);
					 this.quantities.add(quantity);
					 this.cost += price;
					 System.out.println("Added " + product.getName());
					 messageToUser.setText("Added " + product.getName() + " to menu");
					 displayMenuCost();
				 }
				 //If the item is a toping but there is no coffee in the menu, then it wont add the item to the menu
				 //and it will display to the user that the menu must contain coffee
				 else {
					 System.out.println("Your menu must contain coffee to add a topping.");
					 messageToUser.setText("Must contain coffee");
				 }
			 }
			 //If the item is not of type toping and the stock is available then it will add it to the menu 
		 else {
			 this.products.add(product);
			 this.quantities.add(quantity);
			 this.cost += price;
			 System.out.println("Added " + product.getName());
			 messageToUser.setText("Added " + product.getName() + " to menu");
			 displayMenuCost();
		 	}
		 }
		 //If the quantity is not available then the item wont be added and it will display to the user that the quantity isn't available
		 else {
			 System.out.println("Can't add item, exceeds stock.");
			 messageToUser.setText("Quantity too high!");
		 }
	 }
	 
	 /**
	  * This method uses the discount for the user, so it goes into the database and updates the discount value of the user
	  * @param username
	  * @param useDiscount
	  * @throws SQLException
	  */
	 public void useDiscount(String username, int useDiscount) throws SQLException {
		 String eligible = "";
		 useDiscount = 1;
		 try {
			 String SQL = "{call updateDiscountUsed(?,?,?)";
			 CallableStatement cstmt = con.prepareCall(SQL);
			 cstmt.setString(1, username);
			 cstmt.setInt(2, useDiscount);
			 cstmt.registerOutParameter(3, Types.VARCHAR);
			 cstmt.execute();
			 eligible = cstmt.getString(3);
			 con.commit();
		 }catch(Exception e) {
				con.rollback();
				e.printStackTrace();
				System.out.println("USEDISCOUNT: error");
		}
		/*if(eligible.equals("true")) { 
			discountApplied = true;
			 if(cost - 5 < 0) {
				 System.out.println("Menu cost is now 0.");
				 cost = 0;
			 }else {
				 cost-=5;
				 System.out.println("Menu cost is now " + cost);
			 }
		}else {
			System.out.println("Not eligible for a discount.");
		}*/
	 }
	 
	 /**
	  * This method displays the cost of the menu
	  */
	 public void displayMenuCost() {
		 System.out.println("Menu cost: " + cost + "$");
	 }
	 
	 /**
	  * This methods set to true the discountapplied property when the discount is used
	  * @param bol
	  */
	 public void setDiscountApplied(boolean bol) {
		 discountApplied = bol;
	 }
	 /**
	  * This returns a string that contains all the items and their quantity that the menu contains
	  * @return
	  */
	 public String displayMenuItems() {
		 String currentItems = "";
		 for(int i = 0; i < products.size(); i++) {
			 currentItems+=products.get(i).getName() + ": " + quantities.get(i) +", ";
		 }
		 currentItems = currentItems.substring(0, currentItems.length()-2);
		 return currentItems;
	 }
	 
	}


