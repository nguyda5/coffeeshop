/**
 * @author David Nguyen
 * @author Sirine Aoudj
 * This class contains all the info of a product object
 */
package objects;

public class Product {
	private String productID;
	private String name;
	private String type;
	private double retail;
	private int stock;
	
	/**
	 * constructor to create a product
	 * @param productID
	 * @param name
	 * @param type
	 * @param retail
	 * @param stock
	 */
	public Product(String productID, String name, String type, double retail, int stock) {
		this.productID = productID;
		this.name = name;
		this.type = type;
		this.retail = retail;
		this.stock = stock;
	}
	
	/**
	 * Return product id
	 * @return
	 */
	public String getProductId() {
		return this.productID;
	}
	
	/**
	 * @return product name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return product type
	 */
	public String getType() {
		return this.type;
	}
	
	/**
	 * @return product retail
	 */
	public double getRetail() {
		return this.retail;
	}
	
	/**
	 * @return product stock quantity
	 */
	public int getStock() {
		return this.stock;
	}
}


