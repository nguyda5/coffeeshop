/** 
 * @author David Nguyen
 * @author Sirine Aoudj
 * This class contains all the customer information
 */
package objects;

public class Customer {
	private String deliveryAddress;
	private String phoneNumber;
	private String emailAddress;
	private String referred;
	
	/**
	 * This is a Customer constructor that takes as input all of these informations
	 * to create a customer object
	 * @param deliveryAddress
	 * @param phoneNumber
	 * @param emailAddress
	 * @param referred
	 */
	public Customer(String deliveryAddress, String phoneNumber, String emailAddress, String referred) {
		super();
		this.deliveryAddress = deliveryAddress;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
		this.referred = referred;
	}
	/**
	 * This allows to print the customers properties
	 */
	@Override
	public String toString() {
		return "Customer [deliveryAddress=" + deliveryAddress + ", phoneNumber=" + phoneNumber
				+ ", emailAddress=" + emailAddress + ", referred=" + referred + "]";
	}
	
	/**
	 * returns the user delivery address
	 * @return
	 */
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	
	/**
	 * Method allows to set the customers delivery address
	 * @param deliveryAddress
	 */
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	
	/**
	 * returns the user phone number
	 * @return
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	/**
	 * Method allows to set the customers phone number
	 * @param deliveryAddress
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	/**
	 * returns the user email address
	 * @return
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	
	/**
	 * Method allows to set the customers email address
	 * @param deliveryAddress
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	/**
	 * returns the username of the person that referred that customer
	 * @return
	 */
	public String getReferred() {
		return referred;
	}
	/**
	 * Method allows to set the username of the person that referred that customer
	 * @param deliveryAddress
	 */
	public void setReferred(String referred) {
		this.referred = referred;
	}
	
}
