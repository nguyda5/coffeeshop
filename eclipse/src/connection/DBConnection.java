/**
 * @author David Nguyen
 * @Author Sirine Aoudj
 * This class creates a connection to the database
 */
package connection;

import java.sql.*;
import java.util.*;

public class DBConnection {
	public Connection getConnection() {
		Connection con = null;
		Properties ConnProps = new Properties();
		ConnProps.put("user","david");
		ConnProps.put("password","Kingdragon2");
		try {
			con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl",ConnProps);
			con.setAutoCommit(false);
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	public static void main(String[]args) {
		DBConnection dbconnect = new DBConnection();
		if(dbconnect.getConnection()!= null) {
			System.out.println("Successfully connected!");
		}else {
			System.out.println("Connection failed.");
		}
	}
}
