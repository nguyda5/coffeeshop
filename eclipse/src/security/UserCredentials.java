/**
 * @author David Nguyen
 * @author Sirine Aoudj
 * This class contains all the information to create an user and login
 */
package security;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import connection.DBConnection;
import javafx.scene.control.Label;

public class UserCredentials {
	//Use a connection object to call pl/sql
	private Connection con; 
	//Using a secure random to create a salt with he password
	private SecureRandom random = new SecureRandom();
	
	//User credential object constructor takes as input a connection to be initialize
	public UserCredentials(Connection con) throws ClassNotFoundException {
		this.con = con;
	}
	
	/**
	 * This method creates a new user with the information in the database in the user table
	 * @param username 
	 * @param password
	 * @param messageToUser In case this user already exists, it will display a message in the register tab that this user all ready exists
	 * @throws SQLException
	 */
	public void newUser(String username, String password, Label messageToUser) throws SQLException{
		try {
			//Get a salt for the user's password
			String salt = getSalt();
			//Call the procedure createUser to add a new user to the user table
			String query = "{call createUser(?,?,?)}";
			CallableStatement cstmt = con.prepareCall(query);
			cstmt.setString(1,username);
			cstmt.setString(2,salt);
			cstmt.setBytes(3, hash(password,salt));
			cstmt.execute();
			System.out.println("Inserted into USERCREDENTIALS: " + username);
			con.commit();
		}catch(SQLException e) {
			//In case this user already exists, it will display a message in the register tab that this user all ready exists
			con.rollback();
			messageToUser.setText("User already exist!");
			System.out.println("Error: User already exist!");
		}
	}
	
	/**
	 * This method takes a username and password then check in the database if the info is correct
	 * if yes it will return true, else it will return false
	 * @param username
	 * @param password
	 * @return boolean true if the username and password are correct
	 * false if not
	 * @throws SQLException
	 */
	public boolean login(String username, String password) throws SQLException {
		//Use a query that finds in the database user information for that username input
		String query = "SELECT * FROM USERCREDENTIALS WHERE USERNAME = '"+username+"'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		String salt ="";
		byte[] hash = new byte[0];
		while(rs.next()) {
			salt = rs.getString("salt");
			hash = rs.getBytes("hash");
		}
		/*it checks if the username exists
		 * then tt then checks if the password attempt corresponds to the salt and hash that is stored in the database
		 * If not it will return false, if yes then*/ 
		if(salt.length() > 0) {
			byte[] passwordAttempt = hash(password,salt);
			for(int i = 0; i < hash.length; i++) {
				if(!(passwordAttempt[i] == hash[i])) {
					System.out.println("password does not match!");
					return false;
				}
			}
			System.out.println("login successful!");
			return true;
		}
		System.out.println("username does not exist!");
		return false;
	}
	
	/**
	 * This method return a salt that is used when creating a user
	 * @return String salt that is used to store the password 
	 */
	public String getSalt(){
		return new BigInteger(140, random).toString(32);
	}
	
	/**
	 * This method takes a password and a salt then creates hash with these
	 * @param password the users password input
	 * @param salt the salt created for that user
	 * @return hash for the user password
	 */
	public byte[] hash(String password, String salt){
		try{
			SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
			PBEKeySpec spec = new PBEKeySpec( password.toCharArray(), salt.getBytes(), 1024, 256 );

			SecretKey key = skf.generateSecret( spec );
	        byte[] hash = key.getEncoded( );
	        return hash; 
        }catch( NoSuchAlgorithmException | InvalidKeySpecException e ) {
            throw new RuntimeException( e );
        }
	}
	public void insertLoginTime(String username, String time) throws SQLException {
		try {
			String SQL = "{call insertLoginTime(?,?)";
			CallableStatement cstmt = con.prepareCall(SQL);
			cstmt.setString(1, username);
			cstmt.setString(2, time);
			cstmt.execute();
			System.out.println("Insert into LOGINTIME");
			con.commit();
		}catch (SQLException e) {
			con.rollback();
			e.printStackTrace();
		}
	}
	
}
