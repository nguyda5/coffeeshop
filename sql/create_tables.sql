
DROP TABLE menuproducts;
DROP TABLE products;
DROP TABLE menushoppingcart;
DROP TABLE menu;
DROP TABLE customer;
DROP TABLE customer_reference;
DROP TABLE loginTime;
DROP TABLE usercredentials;
DROP TABLE shoppingcart;
DROP TABLE orders;

CREATE TABLE usercredentials(
    user_id VARCHAR2(4) NOT NULL,
    username VARCHAR2(25) NOT NULL,
    salt VARCHAR2(40) NOT NULL,
    hash RAW(120) NOT NULL,
    CONSTRAINT userid_pk PRIMARY KEY (user_id),
    CONSTRAINT username_unique UNIQUE(username));
    
CREATE TABLE logintime(
    user_id VARCHAR2(4) NOT NULL,
    date_time VARCHAR2(20) NOT NULL,
    CONSTRAINT vUserid_fk 
        FOREIGN KEY (user_id)
        REFERENCES usercredentials(user_id));
        

CREATE TABLE customer(
    customer_id VARCHAR2(4) NOT NULL,
    user_id VARCHAR2(4) NOT NULL,
    delivery_address VARCHAR2(50) NOT NULL,
    phone_number VARCHAR2(12),
    email_address VARCHAR2(40),
    discount_used NUMBER,
    CONSTRAINT customerid_pk PRIMARY KEY (customer_id),
    CONSTRAINT userid_fk 
        FOREIGN KEY (user_id)
        REFERENCES usercredentials(user_id),
    CONSTRAINT user_id UNIQUE(user_id));
        
CREATE TABLE customer_reference(
    customer_id VARCHAR2(4) NOT NULL,
    reference_id VARCHAR2(4) NOT NULL,
    CONSTRAINT vCustomerid_fk
        FOREIGN KEY (customer_id)
        REFERENCES customer(customer_id),
    CONSTRAINT unique_referenceID UNIQUE(reference_id));
        
CREATE TABLE menu(
    menu_id VARCHAR2(4) NOT NULL,
    customer_id VARCHAR2(4) NOT NULL,
    CONSTRAINT menuid_pk PRIMARY KEY (menu_id),
    CONSTRAINT customerid_fk 
        FOREIGN KEY (customer_id)
        REFERENCES customer(customer_id));

CREATE TABLE products(
    product_id VARCHAR2(4) NOT NULL,
    name VARCHAR2(25) NOT NULL,
    type VARCHAR2 (20) NOT NULL,
    retail NUMBER(20,2) NOT NULL,
    stock NUMBER(10) NOT NULL,
    CONSTRAINT productid_pk PRIMARY KEY (product_id));
--ALTER TABLE products MODIFY retail NUMBER(20,2);

CREATE TABLE menuproducts(
    menu_id VARCHAR2(4) NOT NULL,
    product_id VARCHAR2(4) NOT NULL,
    quantity NUMBER(5) NOT NULL,
    CONSTRAINT menuid_fk 
        FOREIGN KEY (menu_id)
        REFERENCES menu(menu_id),
    CONSTRAINT productid_fk 
        FOREIGN KEY (product_id)
        REFERENCES products(product_id));

CREATE TABLE orders(
    order_id VARCHAR2(4) NOT NULL,
    order_date DATE NOT NULL,
    delivery_date DATE NOT NULL,
    address VARCHAR2(50) NOT NULL,
    CONSTRAINT orderid_pk PRIMARY KEY(order_id));

CREATE TABLE shoppingcart(
    shoppingcart_id VARCHAR2(4) NOT NULL,
    order_id VARCHAR2(4) NOT NULL,
    CONSTRAINT shoppingcartid_pk PRIMARY KEY (shoppingcart_id),
    CONSTRAINT orderid_fk 
        FOREIGN KEY (order_id)
        REFERENCES orders(order_id));
        
CREATE TABLE menushoppingcart(
    shoppingcart_id VARCHAR2(4) NOT NULL,
    menu_id VARCHAR2(4) NOT NULL,
    quantity NUMBER(5) NOT NULL,
    CONSTRAINT shoppingcartid_fk
        FOREIGN KEY(shoppingcart_id)
        REFERENCES shoppingcart(shoppingcart_id),
    CONSTRAINT vMenuid_fk
        FOREIGN KEY(menu_id)
        REFERENCES menu(menu_id));

/*--FOR TESTING PURPOSES--
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0001','iced coffee','coffee',2.50,100);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0002','hot coffee','coffee',2.00,100);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0003','capuccino','coffee',3.25,100);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0011','sugar','additive',0.00,500);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0001','milk','additive',0.00,500);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0001','cream','additive',0.20,500);

INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0001','iced coffee','coffee',2.50,100);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0002','hot coffee','coffee',2.00,100);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0003','capuccino','coffee,3.25,100);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0011','sugar','additive',0.00,500);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0001','milk','additive',0.00,500);
INSERT INTO products (product_id,name,type,retail,stock)
VALUES ('0001','cream','additive',0.20,500);

INSERT INTO customer (costumer_id,user_id,delivery_address,phone_number,email_address,referred)
VALUES ('1000','0001','8890 rue lemay montreal qc','little_man@gmail.com','1000');
INSERT INTO customer (costumer_id,user_id,delivery_address,phone_number,email_address,referred)
VALUES ('1100','0002','5582 12e avenue qc','little_women@gmail.com','1200');
INSERT INTO customer (costumer_id,user_id,delivery_address,phone_number,email_address,referred)
VALUES ('1200','0003','6687 rue assomption montreal qc','little_kid@gmail.com','1100');

INSERT INTO menuproducts (menu_id,product_id,quantity)
VALUES ('1111','0003',2);
INSERT INTO menuproducts (menu_id,product_id,quantity)
VALUES ('1112','0011',2);

INSERT INTO menushoppingcart (shoppingcart_id,menu_id,quantity)
VALUES ('0100','1111',2);

INSERT INTO shoppingcart (shoppingcart_id,order_id)
VALUES ('0100','0010',2);

INSERT INTO orders (order_id,order_date,deliver_date,adress)
VALUES ('0010',2020-12-11,2020-12-12,'5582 rue assomption qc');*/