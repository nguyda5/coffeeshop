--drop first then create
DROP SEQUENCE sequence_UserID;
DROP FUNCTION idToChar;
DROP PROCEDURE createUser;
DROP FUNCTION getUserId;

DROP SEQUENCE sequence_CustomerID;
DROP PROCEDURE createCustomer;
DROP FUNCTION getCustomerID;
DROP FUNCTION getAddress;
DROP PROCEDURE addReference;
DROP SEQUENCE sequence_MenuID;
DROP PROCEDURE assignCustomerMenu;
DROP TYPE menu_ids;
DROP FUNCTION getMenuID;

DROP SEQUENCE sequence_ProductID;
DROP FUNCTION getProductID;
DROP FUNCTION checkStock;
DROP FUNCTION getRetail;
DROP PROCEDURE updateStock;
DROP FUNCTION checkType;
DROP PROCEDURE createProduct;

DROP PROCEDURE assignProductsMenu;
DROP SEQUENCE sequence_OrderID;
DROP FUNCTION createDeliveryDate;
DROP PROCEDURE createOrder;
DROP SEQUENCE sequence_ShoppingCartID;
DROP PROCEDURE createShoppingCart;
DROP FUNCTION getShoppingCartID;
DROP PROCEDURE assignShoppingCartMenu;
DROP TYPE allQuantity;
DROP FUNCTION getTotalProduct;
DROP TYPE allProductID;
DROP FUNCTION getTotalCost;
DROP PROCEDURE updateDiscountUsed;
DROP FUNCTION getOrderDate;
DROP FUNCTION getDeliveryDate;
DROP PROCEDURE insertLoginTime;

--REGISTER USER
--sequence user id
CREATE SEQUENCE sequence_UserID
    MINVALUE 1
    MAXVALUE 9999
    START WITH 1
    INCREMENT BY 1
    CACHE 20;
/
--id to varchar2 in coffee shop id format
CREATE OR REPLACE FUNCTION idToChar
    (userid IN number)
    RETURN varchar2
IS
    userIdChar VARCHAR2(4) := '0000';
BEGIN
    IF userid < 10 THEN
        userIdChar := CONCAT('000',TO_CHAR(userid));
    ELSIF userid >= 10 AND userid < 100 THEN
        userIdChar := CONCAT('00', TO_CHAR(userid));
    ELSIF userid >= 100 AND userid < 1000 THEN
        userIdChar := CONCAT('0', TO_CHAR(userid));
    ELSIF userid >= 1000 AND userid < 10000 THEN
        userIdChar := TO_CHAR(userid);
    END IF;
    
    RETURN userIdChar;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'IDTOCHAR: An error was encountered!');
END;
/
--procedure create user
CREATE OR REPLACE PROCEDURE createUser(
    vUsername IN usercredentials.username%type,
    vSalt IN usercredentials.salt%type,
    vHash in usercredentials.hash%type)
IS
    vUserid VARCHAR2(4);
BEGIN
    SELECT idToChar(sequence_UserID.NEXTVAL) INTO vUserid FROM dual;
    INSERT INTO usercredentials(user_id,username,salt,hash) VALUES (vUserid,vUsername,vSalt,vHash);
    dbms_output.put_line('Inserted into USERCREDENTIALS: ' || vUserid);
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'CREATEUSER: An error was encountered!');
END;
/
--function get user id
CREATE OR REPLACE FUNCTION getUserId
    (vUsername usercredentials.username%type)
    RETURN VARCHAR2
IS
    vUserId usercredentials.user_id%type;
BEGIN
    SELECT usercredentials.user_id INTO vUserId
        FROM usercredentials
        WHERE username = vUsername;
    RETURN vUserId;
EXCEPTION
    WHEN OTHERS THEN
         raise_application_error(-20001, 'GETUSERID: An error was encountered!');      
END;
/

--sequence customer id
CREATE SEQUENCE sequence_CustomerID
    MINVALUE 1
    MAXVALUE 9999
    START WITH 1
    INCREMENT BY 1
    CACHE 20;
/
--procedure create customer
CREATE OR REPLACE PROCEDURE createCustomer(
    vUserID IN customer.user_id%type,
    vDeliveryAddress IN customer.delivery_address%type,
    vPhoneNumber IN customer.phone_number%type,
    vEmailAddress IN customer.email_address%type,
    vDiscountUsed IN customer.discount_used%type)
IS
    vCustomerID customer.customer_ID%type;
BEGIN
    SELECT idToChar(sequence_CustomerID.NEXTVAL) INTO vCustomerID from dual;
    INSERT INTO customer(customer_id,user_id,delivery_address,phone_number,email_address,discount_used)
        VALUES(vCustomerID,vUserID,vDeliveryAddress,vPhoneNumber,vEmailAddress,vDiscountUsed);
    dbms_output.put_line('Inserted into CUSTOMER: ' || vCustomerID);
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'CREATECUSTOMER: An error was encountered!');
END;
/
BEGIN
    createCustomer('sirine7','','','','','','');
END;
--function get customer
CREATE OR REPLACE FUNCTION getCustomerID
    (vUsername usercredentials.username%type)
    RETURN VARCHAR2
IS
    vCustomerID customer.customer_id%type;
BEGIN
    SELECT customer.customer_id INTO vCustomerID
        FROM usercredentials
        INNER JOIN customer
        USING(user_id)
        WHERE username = vUsername;
    RETURN vCustomerID;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'GETCUSTOMERID: An error was encountered!');
END;
/
--function get address
CREATE OR REPLACE FUNCTION getAddress
    (vUsername usercredentials.username%type)
    RETURN VARCHAR2
IS
    vAddress customer.delivery_address%type;
BEGIN
    SELECT delivery_address INTO vAddress
        FROM usercredentials
        INNER JOIN customer
        USING (user_id)
        WHERE username = vUsername;
    RETURN vAddress;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'GETADDRESS: An error was encountered!');
END;
/
--REFERENCE
--procedure add reference
CREATE OR REPLACE PROCEDURE addReference
    (vCustomerID customer_reference.customer_id%type,
    vReferenceID customer_reference.reference_id%type)
IS
BEGIN
    INSERT INTO customer_reference(customer_id, reference_id)
        VALUES(vCustomerID, vReferenceID);
    dbms_output.put_line('Inserted into CUSTOMER_REFERENCE: ' || vCustomerID);
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'ADDREFERENCE: An error was encountered!');
END;
/
--MENU
--sequence product id
CREATE SEQUENCE sequence_MenuID
    MINVALUE 1
    MAXVALUE 9999
    START WITH 1
    INCREMENT BY 1
    CACHE 20;
/
--procedure assign customer menu
CREATE OR REPLACE PROCEDURE assignCustomerMenu
    (vUsername IN usercredentials.username%type,
    vMenuID OUT menu.menu_id%type)
IS  
    vCustomerID menu.customer_id%type; 
BEGIN
    vCustomerID := getCustomerID(vUsername);
    SELECT idToChar(sequence_MenuID.NEXTVAL) INTO vMenuID from dual;
    INSERT INTO menu(menu_id,customer_id) VALUES (vMenuID,vCustomerID);
    dbms_output.put_line('Inserted into MENU: ' || vMenuID);
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'ASSIGNCUSTOMERMENU: An error was encountered!');
END;
/
--varray menuids
CREATE OR REPLACE TYPE menu_ids AS
    VARRAY(99) OF VARCHAR2(4);
/
--function get menu ids
CREATE OR REPLACE FUNCTION getMenuID
    (vUsername usercredentials.username%type)
    RETURN menu_ids
IS
    vMenuIDS menu_ids;
BEGIN
    SELECT menu_id BULK COLLECT INTO vMenuIDS
        FROM usercredentials
        INNER JOIN customer
        USING (user_id)
        INNER JOIN menu
        USING (customer_id)
        WHERE username = vUsername;
    RETURN vMenuIDS;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'GETMENUID: An error was encountered!');
END;
/
/*This sequence creates the product ids*/
CREATE SEQUENCE sequence_ProductID
    MINVALUE 1
    MAXVALUE 9999
    START WITH 1
    INCREMENT BY 1
    CACHE 20;
/
/*This function takes userId*/    
CREATE OR REPLACE FUNCTION getProductID
    (vName products.name%type)
    RETURN VARCHAR2
IS
    vProductID products.product_id%type;
BEGIN
    SELECT products.product_id INTO vProductID
        FROM products
        WHERE name = vName;
    RETURN vProductID;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'GETPRODUCTID: An error was encountered!');
END;
/
/*This function gives the stock quantity of the product*/
CREATE OR REPLACE FUNCTION checkStock
    (vProductID products.product_id%type)
    RETURN NUMBER
IS
    vStock products.stock%type;
BEGIN
    SELECT products.stock INTO vStock
        FROM products
        WHERE product_id = vProductID;
    RETURN vStock;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'CHECKSTOCK: An error was encountered!');
END;
/
/*This function gives the retail of the product*/
CREATE OR REPLACE FUNCTION getRetail
    (vProductID products.product_id%type)
    RETURN NUMBER
IS
    vRetail products.retail%type;
BEGIN
    SELECT products.retail INTO vRetail
        FROM products
        WHERE product_id = vProductID;
    RETURN vRetail;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'GETRETAIL: An error was encountered!');
END;
/
/*This procedure updates the quantity when an item is bought*/
CREATE OR REPLACE PROCEDURE updateStock(
    vProductID  IN products.product_id%type,
    vQuantity NUMBER)   
IS
   vStock products.stock%type;
BEGIN
    SELECT products.stock INTO vStock
        FROM products
        WHERE product_id = vProductID;
    vStock := vStock - vQuantity;
    UPDATE products
    SET stock = vStock
    WHERE product_id = vProductID;
    
    dbms_output.put_line('Product Updated: ' || vProductID || ' Quantity: ' || vStock );
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'UPDATESTOCK: An error was encountered!');
END;
/
/*This function check if this product is of type coffee or not*/
CREATE OR REPLACE FUNCTION checkType
    (vProductID products.product_id%type,
    vTypeToBeChecked products.type%type)
    RETURN VARCHAR2
IS
    vType products.type%type;
    vContainsType VARCHAR2(20);
BEGIN
    SELECT products.type INTO vType
        FROM products
        WHERE product_id = vProductID;
    
    IF vType = vTypeToBeChecked THEN
    dbms_output.put_line('The product is of type ' || vTypeToBeChecked);
    vContainsType := 'true';
    
    ELSE
    dbms_output.put_line('The product is not of type ' || vTypeToBeChecked);
    vContainsType := 'false';
    
    END IF;
    
    return vContainsType;

EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'CHECKTYPE: An error was encountered!');
END;
/
/*This create a new product with a product_id */
CREATE OR REPLACE PROCEDURE createProduct(
    vName IN products.name%type,
    vType IN products.type%type,
    vRetail IN products.retail%type,
    vStock IN products.stock%type)
IS
    vProductID products.product_id%type;
BEGIN
    SELECT idToChar(sequence_ProductID.NEXTVAL) INTO vProductID from dual;
    INSERT INTO products(product_id,name,type,retail,stock)
        VALUES(vProductID,vName,vType,vRetail,vStock);
    dbms_output.put_line('Inserted into products: ' || vProductID);
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'CREATEPRODUCT: An error was encountered!');
END;
/
--MENU PRODUCTS
--procedure assign products menu
CREATE OR REPLACE PROCEDURE assignProductsMenu(
    vMenuID IN menuproducts.menu_id%type,
    vProductID IN menuproducts.product_id%type,
    vQuantity IN menuproducts.quantity%type)
IS
BEGIN
    INSERT INTO menuproducts(menu_id,product_id,quantity) 
        VALUES(vMenuID,vProductID,vQuantity);
    dbms_output.put_line('Inserted into menuproducts: ' || vMenuID);
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'ASSIGNPRODUCTSMENU: An error was encountered!');
END;
/
--ORDERS
--sequence order id
CREATE SEQUENCE sequence_OrderID
    MINVALUE 1
    MAXVALUE 9999
    START WITH 1
    INCREMENT BY 1
    CACHE 20;
/
--function create delivery date
CREATE OR REPLACE FUNCTION createDeliveryDate
    (vOrderDate DATE)
    RETURN DATE
IS
    vDeliveryDate DATE;
BEGIN
    SELECT (vOrderDate + 1) INTO vDeliveryDate FROM dual;
    RETURN vDeliveryDate;
EXCEPTION
    WHEN OTHERS THEN
        raise_application_error(-20001, 'CREATEDELIVERYDATE: An error was encountered!');
END;
/
--procedure create order
CREATE OR REPLACE PROCEDURE createOrder
    (vUsername IN usercredentials.username%type,
    vOrderID OUT orders.order_id%type)
IS
    vOrderDate orders.order_date%type;
    vDeliveryDate orders.delivery_date%type;
    vAddress orders.address%type;
BEGIN
    SELECT idToChar(sequence_OrderID.NEXTVAL) INTO vOrderID FROM dual;
    vOrderDate := sysdate;
    vDeliveryDate := createDeliveryDate(vOrderDate);
    SELECT delivery_address INTO vAddress
        FROM customer
        INNER JOIN usercredentials
        USING (user_id)
        WHERE username = vUsername;
    INSERT INTO orders(order_id,order_date,delivery_date,address)
        VALUES(vOrderID,vOrderDate,vDeliveryDate,vAddress);
    dbms_output.put_line('Inserted into orders: ' || vOrderID);
EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'CREATEORDER: An error was encountered!');
END;
/
--SHOPPING CART
--sequence shopping cart id
CREATE SEQUENCE sequence_ShoppingCartID
    MINVALUE 1
    MAXVALUE 9999
    START WITH 1
    INCREMENT BY 1
    CACHE 20;
/
--procedure create shopping cart
CREATE OR REPLACE PROCEDURE createShoppingCart
    (vOrderID IN orders.order_id%type,
    vShoppingCartID OUT shoppingcart.shoppingcart_id%type)
IS
BEGIN
    SELECT idToChar(sequence_ShoppingCartID.NEXTVAL) INTO vShoppingCartID FROM dual;
    INSERT INTO shoppingcart(shoppingcart_id, order_id) 
        VALUES (vShoppingCartID, vOrderID);
    dbms_output.put_line('Inserted into shoppingcart: ' || vShoppingCartID);
EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'CREATESHOPPINGCART: An error was encountered!');
END;
/
--function get shopping cart id
CREATE OR REPLACE FUNCTION getShoppingCartID
    (vOrderID shoppingcart.order_id%type)
    RETURN VARCHAR2
IS
    vShoppingCartID shoppingcart.shoppingcart_id%type;
BEGIN
    SELECT shoppingcart_id INTO vShoppingCartID
        FROM shoppingcart
        WHERE order_id = vOrderID;
    RETURN vShoppingCartID;
EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'GETSHOPPINGCARTID: An error was encountered!');    
END;
/
--procedure assign shopping cart menu
CREATE OR REPLACE PROCEDURE assignShoppingCartMenu(
    vShoppingCartID IN menushoppingcart.shoppingcart_id%type,
    vMenuID IN menushoppingcart.menu_id%type,
    vQuantity IN menushoppingcart.quantity%type)
IS
BEGIN
    INSERT INTO menushoppingcart(shoppingcart_id,menu_id,quantity)
        VALUES(vShoppingCartID,vMenuID,vQuantity);
    dbms_output.put_line('Inserted into menushoppingcart:' || vShoppingCartID);
EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'ASSIGNSHOPPINGCARTMENU: An error was encountered!');    
END;
/
--varray allQuantity 
CREATE OR REPLACE TYPE allQuantity AS
    VARRAY(99) OF NUMBER;
/
--function get total product
CREATE OR REPLACE FUNCTION getTotalProduct
    (vShoppingCartID menushoppingcart.shoppingcart_id%type,
    vProductID menuproducts.product_id%type)
    RETURN NUMBER
IS
    vTotalQuantity NUMBER := 0;
    allProductsQt allQuantity;
BEGIN
    SELECT menuproducts.quantity BULK COLLECT INTO allProductsQt
        FROM menuproducts
        INNER JOIN menushoppingcart
        USING (menu_id)
        WHERE shoppingcart_id = vShoppingCartID AND product_id = vProductID;
    FOR i IN 1..allProductsQt.COUNT 
    LOOP
        vTotalQuantity := vTotalQuantity + allProductsQt(i);
    END LOOP;
    RETURN vTotalQuantity;
EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'GETTOTALPRODUCT: An error was encountered!');    
END;
/
--varray allProductID
CREATE OR REPLACE TYPE allProductID AS
    VARRAY(99) OF VARCHAR2(4);
/
CREATE OR REPLACE FUNCTION getTotalCost
    (vShoppingCartID shoppingcart.shoppingcart_id%type)
    RETURN NUMBER
IS
    vAllProductID allProductID;
    vTotalCost NUMBER := 0;
    vRetail NUMBER := 0;
BEGIN
    SELECT UNIQUE product_id BULK COLLECT INTO vAllProductID
        FROM shoppingcart
        INNER JOIN menushoppingcart
        USING (shoppingcart_id)
        INNER JOIN menuproducts
        USING (menu_id)
        WHERE shoppingcart_id = vShoppingCartID;
    FOR i IN 1..vAllProductID.COUNT 
    LOOP
        vRetail := getRetail(vAllProductID(i));
        vTotalCost := vTotalCost + getTotalProduct(vShoppingCartID,vAllProductID(i))*vRetail;
    END LOOP;
    RETURN vTotalCost;
EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'GETTOTALCOST: An error was encountered!');    
END;
/
--procedure update discount used
CREATE OR REPLACE PROCEDURE updateDiscountUsed
    (vUsername IN usercredentials.username%type,
    vDiscountUsed IN customer.discount_used%type,
    vEligible OUT VARCHAR2)
IS
    vOldDiscountUsed customer.discount_used%type;
    vCustomerID customer.customer_id%type;
    vNumOfRef NUMBER;
BEGIN
    vCustomerID := getCustomerID(vUsername);
    SELECT discount_used INTO vOldDiscountUsed
        FROM customer
        INNER JOIN usercredentials
        USING (user_id)
        WHERE username = vUsername;
    SELECT COUNT(reference_id) INTO vNumOfRef
        FROM customer_reference
        WHERE customer_id = vCustomerID;
    IF vOldDiscountUsed < 3 AND vNumOfRef > 0 THEN
        UPDATE customer
        SET discount_used = vOldDiscountUsed + vDiscountUsed
        WHERE customer_id = vCustomerID;
        dbms_output.put_line('Updated discounts used for :' || vCustomerID);
        vEligible := 'true';
    ELSE
        dbms_output.put_line('Customer has reached max amount of discount used!');
        vEligible := 'false';
    END IF;

EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'UPDATEDISCOUNTUSED: An error was encountered!');    
END;
/
--function get order date
CREATE OR REPLACE FUNCTION getOrderDate
    (vOrderID orders.order_id%type)
    RETURN DATE
IS
    vOrderDate orders.order_date%type;
BEGIN
    SELECT order_date INTO vOrderDate
        FROM orders
        WHERE order_id = vOrderID;
    RETURN vOrderDate;
EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'GETORDERDATE: An error was encountered!');      
END;
/
--function get delivery date
CREATE OR REPLACE FUNCTION getDeliveryDate
    (vOrderID orders.order_id%type)
    RETURN DATE
IS
    vDeliveryDate orders.order_date%type;
BEGIN
    SELECT delivery_date INTO vDeliveryDate
        FROM orders
        WHERE order_id = vOrderID;
    RETURN vDeliveryDate;
EXCEPTION
    WHEN OTHERS THEN    
        raise_application_error(-20001, 'GETDELIVERYDATE: An error was encountered!');      
END;
/
--procedure insert login time
CREATE OR REPLACE PROCEDURE insertLoginTime
    (vUsername usercredentials.username%type,
    vDateTime logintime.date_time%type)
IS
    vUserID usercredentials.user_id%type;
BEGIN
    SELECT user_id INTO vUserID
        FROM usercredentials
        WHERE username = vUsername;
     INSERT INTO logintime(user_id, date_time)
        VALUES(vUserID, vDateTime);
    dbms_output.put_line('Inserted into LOGINTIME: ' || vUserID);
EXCEPTION
    WHEN OTHERS THEN    
            raise_application_error(-20001, 'LOGINTIME: An error was encountered!');    
END;






    
